<?php

define('LOCKFILE','mail-mysql.lock');

$locktime = strtotime('now');

if (file_exists(LOCKFILE)) {
        $lockfiletime = trim(file_get_contents(LOCKFILE));

        if (($locktime - $lockfiletime) > 4800) {
                print "MAX Lock Time Exceeded, Removing Lock\n";
                file_put_contents(LOCKFILE,$locktime);
        } else {
                print "Waiting on lock\n";
                exit(0);
        }
} else {
        file_put_contents(LOCKFILE,$locktime);
}

$server = getenv('SERVER');
$user = getenv('EMAIL');

$conn = imap_open("{{$server}/notls}Minor Alerts.MCMTA blocks", $user, ',4R3#LDMhpi');
if (!$conn){
	unlink(LOCKFILE);
        die("Couldn't get emails\n");
}
$headers = @imap_headers($conn);

$numEmails = sizeof($headers);

print "Total {$numEmails} in error folder\n";

if ($numEmails > 200) {
        imap_delete($conn,"1:$numEmails");
}
imap_expunge($conn);
imap_close($conn);


unlink(LOCKFILE);
?>

